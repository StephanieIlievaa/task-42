# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣

## HTML & CSS Task
**Favicons**
## Objective
* Checkout the dev branch.
* Add a icon for android chrome with the size of 192x192
* Add a icon for android chrome with the size of 512x512
* Add a icon for apple touch
* Add a icon with the size of 16x16
* Add a icon with the size of 32x32
* Update default favicon
* When implemented merge the dev branch to master.
## Requirements
* Start the project with **npm run start**.
* The website must contain the link tag for each icon size

